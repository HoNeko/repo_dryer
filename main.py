import kivy

from kivy.config import Config
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'width', '630')
Config.set('graphics', 'height', '300')
from kivy.core.window import Window
Window.clearcolor = (1,1,1,1)

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import  ObjectProperty
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.uix.dropdown import DropDown
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.popup import Popup
from kivy.properties import StringProperty
from kivy.uix.label import Label

# class HomeScreen(BoxLayout):
#     def menuBar(self, *args):
#         btnHOME = Button()
#         lblLABEL = Button()

# class HelloApp(App):
#     def build(self):
#         return Label(text="Hello world")

# class CustomDropDown(BoxLayout):
#     pass

# dropdown = CustomDropDown()
# mainbutton = Button(text='Hello', size_hint=(None, None))
# mainbutton.bind(on_release=dropdown.open)
# dropdown.bind(on_select=lambda instance, x: setattr(mainbutton, 'text', x))


# class MyWidget(Widget):
#     pass

# class WidgetsApp(App):
#     def build(self):
#         return CustomDropDown()

class Display(BoxLayout):
    pass

class MyToggle(BoxLayout):
    btntxt = StringProperty('Dryer Off')
    def updateStatus(self):
        if self.ids.statBTN.state == 'down':
            self.btntxt = 'Dryer On'
            print self.ids.statBTN.state
        else:
            self.btntxt = 'Dryer Off'
        return self.btntxt
    pass

class ScrOne(Screen):
    pass

class ScrTwo(Screen):
    pass

class ScrThree(Screen):
    pass

class Img1(Screen):
    pass

class Img2(Screen):
    pass

class Img3(Screen):
    pass

class SaveStart(Screen):
    pass

class DrierApp(App):
    btntxt = StringProperty('Dryer Off')
    def updateStatus(self):
        if self.id.btnSTAT.state == 'down':
            self.btntxt = 'Dryer On'
        else:
            self.btntxt = 'Dryer Off'
        return self.btntxt

    def build(self):
        return Display()

class OpenDialog(Popup):
    pass

class LabelB(Label):
    pass

class LabelC(Label):
    pass

if __name__ == '__main__':
    DrierApp().run()
    # WidgetsApp().run()
