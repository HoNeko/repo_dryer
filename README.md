# Automated Cacao Dryer Controller
***
This is the software to be used in the Automated Cacao Dryer. This includes control and monitoring of the dryer module via the sensor module.

## Getting Started
***
For Team Members:
***
Fork this repository by clicking the fork button. This gives you a copy of this repository.

Copy the link that can be seen in the Overview tab.

```https://<username@bitbucket.org>/<username>/<repository_name.git>```

On your machine command line go to your desired directory.

```cd <drive letter>:\<env_folder>\<the project>```

Initialize git

```git init```

Then clone it to your machine via

```git clone <repo>```

<repo> is the link that you copied earlier

### Prerequisites
***
Your machine should have the following installed first beforehand:
* ```python 2.7.13``` for installation guides go to [Python Website](https://www.python.org)

### Installing
***
To install this (and start adding code) a virtual environment must be created. From the command line go to the folder containing the project directory.

```cd <drive letter>:\<env_folder>```

Initialize a virtual environment that would contain all the project requirements and plug-ins

```virtualenv <env_name>```

Activate the virtual environment.

(Windows) ```<env_name>\Scripts\activate```
(Linux) ```source bin/activate```

Go to the project directory

```cd <the project>```

Install the requirements

```pip install -r requirements.txt```

This will download the neccessary plug-ins and libraries.

## Running the program for testing
***
Run the program to see in action the code.

```python main.py```

To end the run simply close the application window.

## Deployment
***
[Deployment procedures to be discussed and added soon]

## Built With
***
* [Python](https://www.python.org/) - Programming Language
* [Kivy](https://kivy.org/) - UI framework

## Versioning
***
We use [SemVer](https://semver.org/) for versioning.

## Authors
***
* **Cyril John A. Yaranon** - [LinkedIn](www.linkedin.com/in/cyril-yaranon-a2587b131)
* **Alex Gabriel B. Dagatan**
* **Loisa J. Cachuela**

## License
***
[License pending due to patent application]

## Acknowledgements
***
* Many thanks to code samples from around the web (to be tagged individually soon)
* Inspired by home automation projects and projects from [MagPi](magpi.cc)
